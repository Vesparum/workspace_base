Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, FACEBOOK_CONFIG['app_id'], FACEBOOK_CONFIG['secret'], {:scope => 'user_about_me, publish_actions', :display => 'popup', :image_size => 'normal'}
  
  # # uncomment this if you want to test error handling in dev mode
     configure do |config|
         config.failure_raise_out_environments = []
     end

    on_failure do |env|

        begin

            exception = env['omniauth.error']  
            expected_exception = false

            # This means that the user pressed no when asked if 
            # we could have their e-mail address.
            if (
                    env['omniauth.strategy'].is_a?(OmniAuth::Strategies::Facebook) && 
                    (query = env['rack.request.query_hash']) &&
                    query['error'] == 'access_denied' && 
                    query['error_description'] == 'Permissions error' && 
                    query['error_reason'] == 'user_denied'
                )

                # this will be passed along to the custom_omniauth_callbacks_controller#omniauth_failure, 
                # which will show a nice message to the user
                env['omniauth.error.type'] = "user_denied"
                expected_exception = true

            end

            # we log exceptions using Raven, so we do that here
            # in all cases except with this facebook exception
          #  Raven.capture_exception(exception) unless expected_exception

        # We don't expect any errors above, but even if there
        # is one, we still want to go ahead to the failure
        # endpoint.  So catch all errors here.
        rescue Exception => err
           # Raven.capture_exception(err)
        end

        # do the default behavior
        OmniAuth::FailureEndpoint.call(env)


    end
end