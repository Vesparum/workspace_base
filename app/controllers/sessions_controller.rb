class SessionsController < ApplicationController
    before_filter :validate_not_logged_in, :only => [:new]

    def new 
    end
    
    def create
        user = User.find_by_email(params[:email])
        # If the user exists AND the password entered is correct.
        if user && user.authenticate(params[:password])
          # Save the user id inside the browser cookie. This is how we keep the user 
          # logged in when they navigate around our website.
          session[:user_id] = user.id
          redirect_to '/'
        else
        # If user's login doesn't work, send them back to the login form.
          redirect_to '/login'
        end
    end
    
    def destroy
      session[:user_id] = nil
      redirect_to '/login'
    end
    
    def fb_success
      auth_hash = request.env['omniauth.auth']

      raw_info = auth_hash['extra']['raw_info']
      fib = raw_info['id']
      user = User.find_by_fb_user_id(fib)

      if user
        image = auth_hash['info']['image']
        user.fb_image = image
        user.save
        # Save the user id inside the browser cookie. This is how we keep the user 
        # logged in when they navigate around our website.
        session[:user_id] = user.id
        redirect_to '/'
      else
        # If user does not exist, create account
        @new_user = User.create_from_fb(auth_hash)

        respond_to do |format|
          if @new_user.save(:validate => false)
            session[:user_id] = @new_user.id
            format.html { redirect_to @new_user, notice: 'User was successfully created.' }
          else
            format.html { redirect_to '/auth/failure', notice: 'Error en la creación del usuario' }
        end
    end
        
      end

      @user = User.koala(request.env['omniauth.auth']['credentials'])
    end
    
    def fb_error
    end
end
